# pull official base image
FROM python:3.8-slim-buster

# set work directory
WORKDIR /usr/flask_app

# copy requirements file
COPY ./requirements.txt /usr/flask_app/requirements.txt

# install dependencies
RUN apt-get update && \
    apt-get install -y --no-install-recommends gcc \
    && apt-get install -y uwsgi-plugin-python3

RUN pip install --upgrade pip setuptools wheel \
    && pip install -r /usr/flask_app/requirements.txt \
    && pip install gunicorn \
    && rm -rf /root/.cache/pip

# copy project
COPY . /usr/flask_app/